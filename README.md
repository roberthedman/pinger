# Pinger - A small connectivity checker
This is a simple script which every n (5) seconds pings a list of targets and stores the result in a sql database.

We've been having issues with out network provider so time to start gathering data.

## Setup
You will have to setup your database accordingly and adjust the pinger.py connect script for it.

I did something like this in mine:
```sql
create table pings(dt_entry DATETIME, node VARCHAR(20), target VARCHAR(20), dt DATETIME, connectivity BOOLEAN, PRIMARY KEY (dt, node, target) );
```

The dt_entry is for easy grouping of tables and is set right before the first ping in the list goes out. The dt is when the response is gotten.

You may run this script as a service, you'll find my service file and startscript in this repo.

## Example query
If you want to see how the route out is behaving you can do something like this:
```sql
SELECT dt_entry,MAX(CASE WHEN target = '8.8.8.8' THEN connectivity END) AS "8.8.8.8",     MAX(CASE WHEN target = '192.168.1.1' THEN connectivity END) AS "192.168.1.1" FROM pings WHERE (target = '8.8.8.8' OR target = '192.168.1.1') GROUP BY dt_entry;
```

