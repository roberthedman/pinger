import mariadb
import sys
import os
import schedule
import time

from datetime import datetime


def connect_to_db():
    # Connect to MariaDB Platform
    try:
        conn = mariadb.connect(
            user="robert",
            password="",
            host="10.0.0.251",
            port=3306,
            database="network_tracking"
        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)
    print("Database connection established!")

    # Get Cursor
    cur = conn.cursor()
    return (conn, cur)


def check_targets(cur, conn):
    node_name = "pihole"
    router="10.0.0.1"
    peperouter="192.168.1.1"
    googledns="8.8.8.8"

    targets = [router, peperouter, googledns]
    starttime = datetime.now()
    dt_entry_string = starttime.strftime("%Y-%m-%d %H:%M:%S")
    for target in targets:
        response = os.system("ping -c1 -W2 "+target)
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
        if response == 0:
            alive = True
        else:
            alive = False

        cur.execute(
            "INSERT INTO pings (dt_entry, dt,node,target,connectivity) VALUES (?, ?, ?, ?, ?)", 
            (dt_entry_string, dt_string, node_name, target, alive)
            )

    conn.commit()


(conn, cur) = connect_to_db()

schedule.every(5).seconds.do(check_targets, cur,conn)


while True:
    schedule.run_pending()
    time.sleep(1)

conn.commit()
conn.close()
